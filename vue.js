const mySwaggerClient = {
    url: 'http://localhost:8080/',
    header: {
        'Content-Type': 'application/json',
        'accept': 'application/json',
    },
    async callApi(path, method = 'GET', body = {}) {
        let sq = this.searchedquestions;
        //if(method != method.toUpperCase()) throw 'method value should be Uppercase';
        method = method.toUpperCase();
        let uri = this.url + path;

        let reqinit = {
            method: method, // *GET, POST, PUT, DELETE, etc.
            headers: this.header,
        };

        switch (method) {
            case 'GET':
            case 'DELETE':
                if (Object.keys(body).length > 0) {
                    uri += '?'
                    for (let att in body) {
                        uri += att;
                        uri += '='
                        uri += body[att];
                    }
                }
                return (await fetch(uri, reqinit)).json(); // parses JSON response into native JavaScript objects
                break;
            case 'POST':
            case 'PUT':
                console.log(uri);
                console.log(body);
                Object.defineProperty(reqinit, 'body', {
                    value: JSON.stringify(body)
                })
                return await fetch(uri, reqinit); // parses JSON response into native JavaScript objects
                break;
        }

    },
}

const app = new Vue({
    el: '#app1',
    data: {
        loggedin: false,
        editingQuestion: false,
        editingQuestionId: '',
        posttext: '',
        userData: {},
        showSignIn: true,
        canShowDiv: 3,
        isactive: 3,
        signin: true,
        hideDivForResponsiv: true,
        divreset: false,
        divregistration: false,

        //chat start//
        answertoquestion: ' My favorite singer Michael Jackson ',
        newMessageContent: '',
        messages: [
            { user_id: 1, time: '10:54', content: 'hey, how is it going?' },
            { user_id: 2, time: '12:22', content: 'hey, this is Bob' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 2, time: '12:22', content: 'this is Fathi' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 2, time: '12:22', content: 'this is Fathi' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 2, time: '12:22', content: 'this is Fathi' },
            { user_id: 1, time: '12:22', content: 'this is Fathi' },
            { user_id: 2, time: '12:22', content: 'what are you doing' }
        ],
        isChatting: false,
        actualDiscussion: undefined,
        messageText: '',
        //chat end//
        myquestions: [],
        searchText: '',
        searchedquestions: [],
        allHashtags: [],
        discussions: [],
        discussionUsers: [],

        //answering The Question
        answeringQuestion: false,
        questionToBeAnsweredId: '',
        questionToBeAnsweredText: '',
        questionAnswer: '',
        mobileLayout: {

        }
    },
    created() {

        // console.log('created');
    },
    mounted() {
        this.updateQuestions();
        //this.updateMyQuestions();

        this.routeQuestion();
        window.onhashchange = this.routeQuestion;

        this.$refs["searchInput"].addEventListener("keyup", (event) => {
            switch (event.key) {
                case "Enter":
                    console.log(this.searchText);
                    this.searchQuestions(this.searchText);
                    break;
                case "Escape":
                    this.updateQuestions();
                    break;
            }
        });

        document.addEventListener("keyup", (event) => {
            if (event.key == 'Escape') {
                console.log(event.key);
                this.closeAnswerWindow();
                if (this.editingQuestion)
                    this.stopEditingQuestion();
            }
        });

        //this.updateDiscussions();
        setTimeout(this.updateDiscussions, 5000);
    },
    destroyed() {

        window.removeEventListener("resize", this.resize);
    },
    methods: {
        createUUID() {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            var uuid = s.join("");
            console.log('generatedId: ' + uuid);
            return uuid;
        },
        updateQuestions() {
            mySwaggerClient.callApi('questions', 'get')
                .then(res => {
                    //console.log(res);
                    this.searchedquestions.length = 0;
                    this.searchedquestions.push.apply(this.searchedquestions, res);
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        updateMyQuestions() {
            mySwaggerClient.callApi('questions', 'get', {
                    'userId': this.userData.id,
                })
                .then(res => {

                    this.myquestions.length = 0;
                    if (res != undefined)
                        this.myquestions.push.apply(this.myquestions, res);
                }).catch(error => {
                    this.myquestions = [];
                    console.error('Error:', error);
                });
        },
        searchQuestions(input) {
            mySwaggerClient.callApi('questions', 'get', {
                    'hashtag': input
                })
                .then(res => {
                    console.log(res);
                    this.searchedquestions.length = 0;
                    this.searchedquestions.push.apply(this.searchedquestions, res);
                }).catch(error => {
                    console.error('Error:', error);
                });

        },
        postQuestion() {
            let hashtagRegex = "(?=#)[\\w^#]+\\w";
            let hashtags = Array.from(this.posttext.matchAll("(?=#)[\\w^#]+\\w"), m => m[0]);
            let postTxt = this.posttext;

            hashtags.forEach(element => {
                postTxt = postTxt.replace(element, '');
            })

            postTxt = postTxt.trim();
            hashtags = hashtags.map(element => element = element.replace('#', ''));
            let uuid = this.createUUID();
            let method = 'POST';
            let path = 'question';
            if (this.editingQuestion) {
                uuid = this.editingQuestionId;
                method = 'PUT';
                this.editingQuestion = false;
                path += '?questionId=' + this.editingQuestionId;
            }

            let reqBody = {
                "id": uuid,
                "text": postTxt,
                "hashtags": hashtags,
                "manager_id": this.userData.id,
            };


            mySwaggerClient.callApi(path, method, reqBody)
                .then(res => {
                    this.posttext = '';
                    this.updateQuestions();
                    this.updateMyQuestions();
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        updateQuestion(id) {
            //let uuid = this.myquestions[index].id;
            let uuid = id;
            const reqBody = {
                "id": uuid,
                "text": this.posttext,
                "hashtags": this.hashtags,
                "manager_id": this.userData.id,
            };

            mySwaggerClient.callApi('questions', 'PUT', reqBody)
                .then(res => {
                    console.log(res);
                }).catch(error => {
                    console.error('Error:', error);
                });

        },
        deleteQuestion(index) {
            console.log("deleting question: " + index);
            let id = this.myquestions[index].id;
            console.log('deleting : ' + id);
            mySwaggerClient.callApi('question', 'DELETE', {
                    'questionId': id
                })
                .then(res => {
                    this.updateMyQuestions();
                    this.updateQuestions();

                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        registerUser(em, username, pwd) {
            let uuid = this.createUUID();
            console.log(uuid);
            const body = {
                'id': uuid,
                'username': username,
                'email': em,
                'full_name': username + " " + em,
                'created_at': "2021-08-02T21:56:02.077Z",
            };
            mySwaggerClient.callApi('user', 'POST', body)
                .then(res => {
                    app.updateUserData(em);
                    // console.log(res);
                    // console.log('user wurde regestiert');
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        updateUserData(email) {

            mySwaggerClient.callApi('user', 'GET', {
                    'email': email
                })
                .then(data => {
                    // console.log(data);
                    this.userData = data;
                    this.updateMyQuestions();
                    app.updateDiscussions();
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        copyLink(index, fromMyQuestions) {
            const id = (fromMyQuestions ? this.myquestions[index].id : this.searchedquestions[index].id);
            const url = window.location.href;
            const hash = window.location.hash;
            const index_of_hash = url.indexOf(hash) || url.length;
            const hashless_url = url.substr(0, index_of_hash);
            const link = hashless_url + '#' + this.searchedquestions[index].id;
            //console.log(link + "wurde im Clipboard abgespeichert.");
            var TempText = document.createElement("input");
            TempText.value = link;
            document.body.appendChild(TempText);
            TempText.select();
            document.execCommand("copy");
            document.body.removeChild(TempText);
        },
        getHashTags(post) {
            let hashtagRegex = "(?=#)[\\w^#]+\\w";
            let hashtags = Array.from(this.posttext.matchAll("(?=#)[\\w^#]+\\w"), m => m[0]);
            let postTxt = this.posttext;
            hashtags.forEach(element => {
                postTxt = postTxt.replace(element, '');
            })
            postTxt = postTxt.trim();

            console.log(postTxt);
            console.log(hashtags);
        },
        editQuestion(index) {
            this.editingQuestion = true;
            this.editingQuestionId = this.myquestions[index].id;
            this.posttext = this.myquestions[index].text;
            this.posttext += '\n';
            this.myquestions[index].hashtags.forEach(ht => this.posttext += '#' + ht + ' ');
        },
        updateDiscussions() {
            mySwaggerClient.callApi('discussions', 'GET', { 'userId': this.userData.id })
                .then(dis => {
                    dis.forEach(element => {
                        const otherUserData = element.users[0].user_id == this.userData.id ? element.users[1] : element.users[0];
                        console.log(element.users);
                        const otherUserId = otherUserData.user_id;
                        const otherUserAnswerId = otherUserData.answer_id;
                        const discussionDataObject = {
                            id: element.id,
                            question: {},
                            user: {},
                            myAnswer: "",
                            otherUserAnswer: "",
                            deleting: {
                                deleted: false,
                                remainedTime: 5,
                                deleteFunction: null,
                            },
                            messages: [],
                            onMessagesUpdate: (snapshot) => {
                                discussionDataObject.messages.push(snapshot.val());
                            },
                            onInit: (data) => {
                                let valuesObject = data.val();
                                for (let key in valuesObject) {
                                    discussionDataObject.messages.push(valuesObject[key]);
                                }
                            },
                        };


                        mySwaggerClient.callApi('user', 'GET', {
                                'userId': otherUserId
                            })
                            .then(user => {
                                discussionDataObject.user = user;
                                mySwaggerClient.callApi('question', 'get', {
                                        questionId: element.question_id
                                    })
                                    .then(question => {
                                        discussionDataObject.question = question;
                                        mySwaggerClient.callApi('answer', 'get', {
                                                answerId: otherUserData.answer_id
                                            })
                                            .then(answer => {
                                                discussionDataObject.otherUserAnswer = answer;
                                                if (this.discussions.filter(item => item.id == element.id).length == 0) {
                                                    //Messanger.initChat(discussionDataObject.id, discussionDataObject.onInit);
                                                    Messanger.addChat(discussionDataObject.id, discussionDataObject.onMessagesUpdate);
                                                    this.discussions.push(discussionDataObject);

                                                }
                                            }).catch(error => {
                                                console.error('Error:', error);
                                            });

                                    }).catch(error => {
                                        console.error('Error:', error);
                                    });

                            }).catch(error => {
                                console.error('Error:', error);
                            });



                    })
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        createDiscussion() {
            const d = {
                "id": this.createUUID(),
                "question_id": this.myquestions[0],
                "users": [{
                        "user_id": this.userData.id,
                        "answer_id": this.createUUID(),
                        "firebase_tokens": [
                            "string"
                        ],
                        "left": true
                    },
                    {
                        "user_id": "98c96d2d-c934-41bc-a53f-b3f685c9d266",
                        "answer_id": this.createUUID(),
                        "firebase_tokens": [
                            "string"
                        ],
                        "left": true
                    }
                ]
            };

            mySwaggerClient.callApi('discussion', 'post', d);
        },
        updateHashtags() {

            mySwaggerClient.callApi('hashtags', 'GET')
                .then(data => {
                    console.log(data);
                    this.allHashtags = data;
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        answerQuestion(index, fromMyQuestions = false) {
            if (!this.loggedin) return;
            this.answeringQuestion = true;
            const questionToBeAnswered = fromMyQuestions ? this.myquestions[index] : this.searchedquestions[index];
            this.questionToBeAnsweredId = questionToBeAnswered.id;
            this.questionToBeAnsweredText = questionToBeAnswered.text;
        },
        routeQuestion() {
            const id = window.location.hash.substring(1);
            if (id.match("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$") === null) return;
            const questionToBeAnswered = mySwaggerClient.callApi('question', 'GET', {
                    'questionId': id
                })
                .then(data => {
                    console.log(data);
                    this.answeringQuestion = true;
                    this.questionToBeAnsweredId = data.id;
                    this.questionToBeAnsweredText = data.text;
                }).catch(error => {
                    console.error('Error:', error);
                });
        },
        postAnswer() {
            const reqBody = {
                "id": this.createUUID(),
                "text": this.questionAnswer,
                "question_id": this.questionToBeAnsweredId,
                "user_id": this.userData.id,
                "firebase_tokens": [
                    "string"
                ]
            };
            console.log(reqBody);
            mySwaggerClient.callApi('answer', 'post', reqBody)
                .then(res => {
                    this.closeAnswerWindow();
                }).catch(error => {
                    console.log('error');
                    console.error('Error:', error);
                });
        },
        closeAnswerWindow() {
            this.questionAnswer = '';
            this.answeringQuestion = false;
        },
        stopEditingQuestion() {
            this.editingQuestion = false;
            this.posttext = '';
        },
        deleteDiscussion(disId) {
            let dis = this.discussions.find(discussion => discussion.id == disId);
            let allDiscussions = this.discussions;
            dis.deleting.deleted = true;
            dis.deleting.deleteFunction = setInterval(function() {
                dis.deleting.remainedTime--;
                if (dis.deleting.remainedTime == 0) {
                    mySwaggerClient.callApi('discussion', 'DELETE', {
                        'discussionId': disId,
                    }).then(res => {});
                    allDiscussions.splice(allDiscussions.indexOf(dis), 1);
                    clearInterval(dis.deleting.deleteFunction);

                }
            }, 1000);
        },
        undoDeletingDiscussion(disId) {
            console.log("undo deleting: " + disId);
            dis = this.discussions.find(discussion => discussion.id == disId);
            clearInterval(dis.deleting.deleteFunction);
            dis.deleting.deleted = false;
            dis.deleting.remainedTime = 5;
        },
        sendMessage() {
            const messageObject = {
                user_id: this.userData.id,
                time: (new Date()).toLocaleTimeString(),
                content: this.messageText,
            };
            this.messageText = "";
            Messanger.sendMessage(messageObject, this.actualDiscussion.id);

            console.log("sending " + messageObject.content);
        },
        startChatting(discussion) {
            this.isChatting = true;
            this.actualDiscussion = discussion;
            this.messages = discussion.messages;
        },
        isSelected(val) {
            this.isactive = this.canShowDiv = val;
            if (val == 4) {
                this.signin = true;
                this.divreset = false;
                this.divregistration = false;

            }

            if (val == 5) {
                this.signin = false;
                this.divreset = false;
                this.divregistration = true;

            }
            this.hideDivForResponsiv = false;

            if (val == 3) {
                this.hideDivForResponsiv = true;
            }

        },
    }
});