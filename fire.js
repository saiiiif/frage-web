var config = {
    apiKey: "AIzaSyBALQyISeEsYpS1SCWu-tROPMBM0pCsTRM",
    authDomain: "frage-316017.firebaseapp.com",
    databaseURL: "https://frage-316017-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "frage-316017",
    storageBucket: "frage-316017.appspot.com",
    messagingSenderId: "9063758064",
    appId: "1:9063758064:web:aeaa8282b2180dcdd12a35",
    measurementId: "G-GF9PESEGDD"
};


firebase.initializeApp(config);
const auth = firebase.auth();

const Messanger = {

    init: function() {

    },
    sendMessage(message, chatId) {
        firebase.database().ref(chatId).push().set(message);

    },
    addChat(chatId, chatUpdateMethode) {
        firebase.database().ref(chatId).on("child_added", chatUpdateMethode);
    },
    initChat(chatId, loadChatMethode) {
        firebase.database().ref(chatId).once("value", loadChatMethode);
    }
}
app.appMessanger = Messanger;

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // User is signed in.
        // console.log("loggedin");
        app.loggedin = true;
        var user = firebase.auth().currentUser;

        if (user != null) {

            app.userEmail = user.email;
            app.updateUserData(user.email);
        }

    } else {
        // No user is signed in.
        app.loggedin = false;
    }
});

function login1() {

    var userEmail = document.getElementById("input0").value;
    var userPass = document.getElementById("input1").value;


    firebase.auth().signInWithEmailAndPassword(userEmail, userPass)
        .then(data => {
            app.updateUserData(userEmail);

        })
        .catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;

            window.alert("Error : " + errorMessage);

        });

}

function logout1() {
    firebase.auth().signOut();
    document.getElementById("info").style.display = "none";

    //clear inputs
    document.getElementById('input0').value = '';
    document.getElementById('input1').value = '';

}

function signUp1() {

    var email = document.getElementById("input0_r");
    var password = document.getElementById("input2-r");
    var username = document.getElementById("username");
    const promise = auth.createUserWithEmailAndPassword(email.value, password.value);

    promise.then(e => {
        app.registerUser(email.value, username.value, password.value);
    });
    promise.catch(e => alert(e.message));

    alert("Signed Up");
}

const resetPassword = document.getElementById('resetPassword');
auth.languageCode = 'DE_de';

auth.useDeviceLanguage();


function PasswordReset() {
    const mailField = document.getElementById('input_pass');
    const email = mailField.value;

    auth.sendPasswordResetEmail(email)
        .then(() => {
            console.log('Password Reset Email Sent Successfully!');
            window.alert('Password Reset Email Sent Successfully!');
            document.getElementById('input_pass').value = '';

        })
        .catch(error => {
            console.error(error);
        })
}