function show_registration() {

    app.signin = false;
    app.divreset = false;
    app.divregistration = true;
}

function show_login() {

    app.signin = true;
    app.divreset = false;
    app.divregistration = false;
}

function show_PasswordReset() {

    app.signin = false;
    app.divreset = true;
    app.divregistration = false;
}